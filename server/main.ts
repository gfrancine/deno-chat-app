import { parseConfig } from "./parse-config.ts";
import { ChatServer } from "./server.ts";

await new ChatServer(parseConfig()).start();
