import { extname, join } from "https://deno.land/std@0.113.0/path/mod.ts";

export type Resource = {
  data: Uint8Array;
  contentType?: string;
};

const EXT_CTYPE_MAP: Record<string, string> = {
  html: "text/html",
  css: "text/css",
};

const DIR_INDEX_NAMES: Set<string> = new Set(["index.html"]);

export class StaticAssets {
  private resources: Record<string, Resource> = {};

  fromDirectory(path: string) {
    const recurse = (root: string, dirPath: string) => {
      for (const entry of Deno.readDirSync(dirPath)) {
        if (entry.isFile) {
          const resource = {
            data: Deno.readFileSync(join(dirPath, entry.name)),
            contentType: EXT_CTYPE_MAP[extname(entry.name).slice(1)],
          };

          this.resources[root + entry.name] = resource;

          if (DIR_INDEX_NAMES.has(entry.name)) {
            this.resources[root] = resource;
          }
        } else {
          recurse(root + entry.name + "/", dirPath);
        }
      }
    };

    recurse("/", path);
  }

  respond(resourcePath: string) {
    const resource = this.resources[resourcePath];
    if (resource) {
      const headers = resource.contentType
        ? {
            "Content-type": resource.contentType,
          }
        : undefined;

      return new Response(resource.data, {
        headers,
      });
    }

    const resource404 = this.resources["/404.html"];
    if (resource404) {
      return new Response(resource404.data, {
        status: 404,
        headers: {
          "Content-type": EXT_CTYPE_MAP.html,
        },
      });
    }

    return new Response(undefined, {
      status: 404,
    });
  }
}
