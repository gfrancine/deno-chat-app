import { parse } from "https://deno.land/std@0.95.0/flags/mod.ts";

export type Config = {
  port: number;
  assetsPath: string;
};

export function parseConfig(): Config {
  const parsedArgs = parse(Deno.args);

  return {
    port: parsedArgs.port || 3000,
    assetsPath: parsedArgs.assets || "public",
  };
}
