import { nanoid } from "https://deno.land/x/nanoid@v3.0.0/mod.ts";
import { decode, encode } from "../common/messages.ts";

export type Client = {
  id: string;
  socket: WebSocket;
};

const INACTIVE_ROOM_MS = 24 * 60 * 60 * 1000;
const CYCLE_INTERVAL_MS = 60 * 60 * 1000;

export class Room {
  private lastActivity = Date.now();
  private clients: Record<string, Client> = {};

  private updateActivity() {
    this.lastActivity = Date.now();
  }

  isInactive() {
    return (
      Date.now() - this.lastActivity >= INACTIVE_ROOM_MS &&
      Object.values(this.clients).length < 1
    );
  }

  addSocket(socket: WebSocket, clientId?: string) {
    this.updateActivity();

    if (clientId && this.clients[clientId]) {
      socket.close(1000, "Client ID already exists");
      return;
    }

    const client = {
      id: clientId || nanoid(),
      socket,
    };

    this.clients[client.id] = client;

    socket.onclose = () => {
      delete this.clients[client.id];
      this.broadcast(
        encode({
          type: "userLeft",
          name: "User " + client.id.slice(0, 8),
        })
      );
    };

    socket.onopen = () => {
      this.broadcast(
        encode({
          type: "userJoined",
          name: "User " + client.id.slice(0, 8),
        })
      );

      if (!clientId)
        socket.send(
          encode({
            type: "assignClientId",
            id: client.id,
          })
        );
    };

    socket.onmessage = this.makeSocketMessageHandler(client);

    return client;
  }

  private broadcast(message: string) {
    this.updateActivity();

    for (const client of Object.values(this.clients)) {
      client.socket.send(message);
    }
  }

  private makeSocketMessageHandler(client: Client) {
    return (e: MessageEvent) => {
      this.updateActivity();

      const result = decode(e.data);
      if (!result.success) return;

      if (result.message.type === "chatByClient") {
        if (result.message.text.trim().length < 1) return;
        this.broadcast(
          encode({
            type: "chatByClientFromServer",
            name: "User " + client.id.slice(0, 8),
            text: result.message.text,
          })
        );
      }
    };
  }
}

export class RoomRegistry {
  private rooms: Record<string, Room> = {};

  addSocketToRoom(id: string, socket: WebSocket, clientId?: string) {
    let room = this.rooms[id];
    if (!room) {
      room = new Room();
      this.rooms[id] = room;
    }
    room.addSocket(socket, clientId);
  }

  startCleanCycle() {
    setInterval(() => {
      for (const [id, room] of Object.entries(this.rooms)) {
        if (room.isInactive()) delete this.rooms[id];
      }
    }, CYCLE_INTERVAL_MS);
  }
}
