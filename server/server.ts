import { Config } from "./parse-config.ts";
import { StaticAssets } from "./static-assets.ts";
import { urlParse } from "https://deno.land/x/url_parse@1.0.0/mod.ts";
import { listenAndServe } from "https://deno.land/std@0.113.0/http/server.ts";
import { RoomRegistry } from "./rooms.ts";

export class ChatServer {
  private config: Config;
  private staticAssets: StaticAssets;
  private roomRegistry = new RoomRegistry();

  constructor(config: Config) {
    this.config = config;
    this.staticAssets = new StaticAssets();
    this.staticAssets.fromDirectory(config.assetsPath);
  }

  async start() {
    console.log(`Serving on port ${this.config.port}`);

    this.roomRegistry.startCleanCycle();

    await listenAndServe(":" + this.config.port, (r: Request) => {
      const parsedUrl = urlParse(r.url);
      const roomId = parsedUrl.searchParams.get("room");
      const clientId = parsedUrl.searchParams.get("clientId");

      if (roomId && parsedUrl.pathname === "/ws") {
        const { socket, response } = Deno.upgradeWebSocket(r);
        this.roomRegistry.addSocketToRoom(
          roomId,
          socket,
          clientId || undefined
        );
        return response;
      }

      return this.staticAssets.respond(parsedUrl.pathname);
    });
  }
}
