export type Message =
  // client requests to send a message to the room
  | {
      type: "chatByClient";
      text: string;
    }
  // server-validated chat message with the client's id
  | {
      type: "chatByClientFromServer";
      text: string;
      name: string;
    }
  | {
      type: "userJoined";
      name: string;
    }
  | {
      type: "userLeft";
      name: string;
    }
  | {
      type: "assignClientId";
      id: string;
    };

export type MessageType = Message["type"];

export const MESSAGE_TYPES = new Set<MessageType>([
  "chatByClient",
  "chatByClientFromServer",
  "userJoined",
  "userLeft",
  "assignClientId",
]);

export type DecodeResult =
  | { success: true; message: Message }
  | { success: false };

const HEADER = "chatapp";

export function encode(data: Message) {
  return HEADER + JSON.stringify(data);
}

export function decode(encoded: string): DecodeResult {
  if (!encoded.startsWith(HEADER)) return { success: false };
  const message = JSON.parse(encoded.slice(HEADER.length));

  if (
    typeof message === "object" &&
    message !== null &&
    MESSAGE_TYPES.has(message.type)
  ) {
    return {
      success: true,
      message,
    };
  }

  return { success: false };
}
