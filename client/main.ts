import { nanoid } from "https://deno.land/x/nanoid@v3.0.0/mod.ts";
import { urlParse } from "https://deno.land/x/url_parse@1.0.0/mod.ts";
import { ChatClient } from "./client.ts";

(() => {
  const parsedLocation = new URL(location.href);
  const roomId = parsedLocation.searchParams.get("room");

  if (!roomId) {
    location.href = urlParse({
      protocol: location.protocol,
      hostname: location.host,
      port: location.port,
      query: [
        {
          key: "room",
          value: nanoid(),
        },
      ],
    }).toString();
    return;
  }

  const client = new ChatClient(roomId);
  client.socketEvents.on("message", console.log);

  const q = (query: string) => document.querySelector(query);
  const input = q("#chat-input") as HTMLTextAreaElement;
  const send = q("#chat-send") as Element;
  const messages = q("#chat-messages") as Element;
  const messagesContainer = q("#chat-messages-container") as Element;

  const sendChat = () => {
    client.sendChat(input.value);
    input.value = "";
    input.focus();
  };

  send.addEventListener("click", sendChat);

  input.addEventListener("keydown", (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      sendChat();
    }
  });

  const addMessage = (kind: "system" | "chat", text: string) => {
    const messageEl = document.createElement("pre");
    messageEl.innerText = text;
    messageEl.classList.add("message");
    messageEl.classList.add(kind);
    messages.appendChild(messageEl);
    messagesContainer.scrollTop = messages.scrollHeight;
  };

  client.socketEvents.on("open", () => {
    addMessage("system", `Connected!`);
  });

  client.socketEvents.on("error", () => {
    addMessage("system", `A connection error occurred.`);
  });

  client.socketEvents.on("close", () => {
    addMessage("system", `Connection closed.`);
  });

  client.socketEvents.on("retry", () => {
    addMessage("system", `Connection closed, reconnecting...`);
  });

  client.socketEvents.on("message", (message) => {
    switch (message.type) {
      case "chatByClientFromServer": {
        addMessage("chat", `${message.name}: ${message.text}`);
        break;
      }
      case "userJoined": {
        addMessage("system", `${message.name} joined the room.`);
        break;
      }
      case "userLeft": {
        addMessage("system", `${message.name} disconnected.`);
        break;
      }
    }
  });
})();
