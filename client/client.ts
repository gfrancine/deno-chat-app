import { urlParse } from "https://deno.land/x/url_parse@1.0.0/mod.ts";
import { decode, encode, Message } from "../common/messages.ts";
import { Emitter } from "./emitter.ts";

const WS_PROTOCOL = location.protocol === "https:" ? "wss://" : "ws://";

export class ChatClient {
  private wsUrl: string;
  private socket: WebSocket;
  private reconnectRetries = 0;
  private clientId?: string;
  private roomId: string;
  socketEvents: Emitter<{
    message: (message: Message) => unknown;
    open: () => unknown;
    retry: () => unknown;
    close: () => unknown;
    error: () => unknown;
  }> = new Emitter();

  constructor(roomId: string) {
    this.roomId = roomId;
    this.wsUrl = this.getWsUrl();
    this.socket = this.createSocket();
  }

  private getWsUrl() {
    const queries = [
      {
        key: "room",
        value: this.roomId,
      },
    ];

    if (this.clientId) {
      queries.push({
        key: "clientId",
        value: this.clientId,
      });
    }

    return urlParse({
      protocol: WS_PROTOCOL,
      hostname: location.host,
      port: location.port,
      pathname: "/ws",
      query: queries,
    }).toString();
  }

  private createSocket() {
    const socket = new WebSocket(this.wsUrl);

    socket.onmessage = (e) => {
      const result = decode(e.data);
      if (!result.success) return;
      if (result.message.type === "assignClientId") {
        console.log(
          "assign client id",
          result.message.id,
          "current id",
          this.clientId
        );
        this.clientId = result.message.id;
        this.wsUrl = this.getWsUrl();
      }
      this.socketEvents.emit("message", result.message);
    };

    socket.onclose = () => {
      if (this.reconnectRetries === 5) {
        this.socketEvents.emit("close");
        return;
      }
      this.socketEvents.emit("retry");
      this.reconnectRetries++;
      setTimeout(() => {
        this.socket = this.createSocket();
      }, 2000);
    };

    socket.onopen = () => {
      this.reconnectRetries = 0;
      this.socketEvents.emit("open");
    };

    socket.onerror = () => this.socketEvents.emit("error");
    return socket;
  }

  sendChat(text: string) {
    this.socket.send(
      encode({
        type: "chatByClient",
        text,
      })
    );
  }
}
